# External Import 
import argparse                   # To parse command line arguments
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer   # Import Kafka producder
from kafka import TopicPartition
import CascadePropertiesConsumer
from sklearn.ensemble import RandomForestRegressor
import pickle


#logger = logger.get_logger("Learner", CascadePropertiesConsumer.args.broker_list)

consumer = KafkaConsumer('sample', bootstrap_servers = CascadePropertiesConsumer.args.broker_list, 
value_deserializer=lambda v: json.loads(v.decode('utf-8')),
key_deserializer=lambda x : int.from_bytes(x, "big"))

producer = KafkaProducer(bootstrap_servers = CascadePropertiesConsumer.args.broker_list,
                        key_serializer=lambda x : x.to_bytes(2, byteorder='big'),
                        value_serializer=lambda m: pickle.dumps(m))

T_obs = CascadePropertiesConsumer.args.obs_windows

X = dict.fromkeys(T_obs, [])
W = dict.fromkeys(T_obs, [])

L_when_to_send=10

for msg in consumer :
    T_obs=msg.key
    msg=msg.value
    X[T_obs].append(msg['X'])
    W[T_obs].append(msg['W'])
    if len(X[T_obs])%10 == 0 :
        regr= RandomForestRegressor()
        forest = regr.fit(X[T_obs],W[T_obs])
        producer.send('model', key=T_obs, value=forest) # Sending the new forest as a pickle.

