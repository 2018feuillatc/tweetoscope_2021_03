## To run the tweet generator:

1. From within the repository, execute the command `docker build -t tweet_generator .` <br>
2. Launch a zookeeper container with the command : `docker run -it --network host zookeeper`
3. Execute the command `docker run --name=myTweetGenerator tweet_generator ./tweet-generator params.config`

## Non mac users
Before building from the `Dockerfile`, be sure to modify the `params.config` file. At the end, the line concerning the kafka brokers must be uncommented (and the older one commented).



